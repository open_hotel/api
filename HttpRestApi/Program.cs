using Core.Entities;
using Core.Helpers.Mapping.Configurations;
using Core.Persistence;
using Core.Services;
using Infrastructure.DbContexts;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using OpenHotelRestApi.Helpers.Converters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.Configure<JsonOptions>(options =>
{
    // Custom Json Parsers/serializers
    options.JsonSerializerOptions.Converters.Add(new DateOnlyConverter());
});

builder.Services.AddDbContext<ReservationContext>();

builder.Services.AddAutoMapper(typeof(AutoMapperConfig));

builder.Services.AddScoped<IReservationService, ReservationService>();
builder.Services.AddScoped<ICrudRepository<Reservation>, ReservationRepository>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.MapType(typeof(DateOnly), () => new OpenApiSchema
    {
        Type = "string",
        Example = new OpenApiString("2021-12-31"),
    });
});

builder.Services.AddCors(cors => { cors.AddDefaultPolicy(policy => { policy.WithOrigins("*"); }); });

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseCors();

app.UseAuthorization();

app.MapControllers();

app.Run();
