using System.Text.Json;
using System.Text.Json.Serialization;

namespace OpenHotelRestApi.Helpers.Converters;

public class DateOnlyConverter : JsonConverter<DateOnly>
{
    public override DateOnly Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return DateOnly.Parse(reader.GetString());
    }

    public override void Write(Utf8JsonWriter writer, DateOnly date, JsonSerializerOptions options)
        => writer.WriteStringValue($"{date.Year:D4}-{date.Month:D2}-{date.Day:D2}");
}
