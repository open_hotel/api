using Core.Dto.Reservation;
using Core.Services;

namespace OpenHotelRestApi.Controllers.Reservation;

public class ReservationController : CrudController<ReservationDto>
{
    public ReservationController(IReservationService service) : base(service)
    {
    }
}
