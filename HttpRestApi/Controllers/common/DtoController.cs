using Core.Dto;
using Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace OpenHotelRestApi.Controllers;

[Route("[controller]")]
public abstract class DtoController<TDto> : OpenHotelController<IDtoService<TDto>>
    where TDto : OpenHotelDto
{
    protected DtoController(IDtoService<TDto> service) : base(service)
    {
        Service = service;
    }
}
