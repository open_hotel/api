using Core.Dto;
using Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace OpenHotelRestApi.Controllers;

public class CrudController<TDto> : DtoController<TDto>, ICrudController<TDto>
    where TDto : OpenHotelDto
{
    private readonly ICrudService<TDto> service;

    public CrudController(ICrudService<TDto> service) : base(service)
    {
        this.service = service;
    }

    [HttpPost]
    public virtual async Task<ActionResult<TDto>> Create(TDto dto)
    {
        await service.Create(dto);
        return new CreatedResult("/", dto);
    }

    [HttpGet("{id:guid}")]
    public virtual async Task<ActionResult<TDto>> GetOne(Guid id)
    {
        var dto = await service.GetOne(id);
        return new ActionResult<TDto>(dto);
    }

    [HttpGet]
    public virtual async Task<ActionResult<IEnumerable<TDto>>> GetAll()
    {
        var dtoList = await service.GetAll();
        return new ActionResult<IEnumerable<TDto>>(dtoList);
    }

    [HttpPut]
    public virtual async Task<ActionResult<TDto>> Update(TDto existingObject)
    {
        var dto = await service.Update(existingObject);
        return new ActionResult<TDto>(dto);
    }

    [HttpDelete]
    public virtual async Task<ActionResult<TDto>> Delete(TDto existingObject)
    {
        await service.Delete(existingObject);
        return new NoContentResult();
    }
}
