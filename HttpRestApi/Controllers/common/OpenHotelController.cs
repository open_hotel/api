using Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace OpenHotelRestApi.Controllers;

[ApiController]
public abstract class OpenHotelController<TService> : ControllerBase
    where TService : IOpenHotelService
{
    protected TService Service { get; init; }

    public OpenHotelController(TService service)
    {
        Service = service;
    }
}
