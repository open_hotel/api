using Core.Dto;
using Microsoft.AspNetCore.Mvc;

namespace OpenHotelRestApi.Controllers;

public interface ICrudController<TDto> where TDto : OpenHotelDto
{
    public Task<ActionResult<TDto>> Create(TDto newObject);
    public Task<ActionResult<TDto>> GetOne(Guid id);
    public Task<ActionResult<IEnumerable<TDto>>> GetAll();
    public Task<ActionResult<TDto>> Update(TDto existingObject);
    public Task<ActionResult<TDto>> Delete(TDto existingObject);
}
