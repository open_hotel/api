using Microsoft.AspNetCore.Mvc;

namespace OpenHotelRestApi.Controllers;

[ApiController]
[Route("/")]
public class RootPathController
{
    [HttpGet]
    public IActionResult Get()
    {
        return new EmptyResult();
    }
}
