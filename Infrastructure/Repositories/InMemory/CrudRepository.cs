using Core.Entities;
using Core.Persistence;

namespace Infrastructure.Repositories.InMemory;

public class CrudRepository<TEntity> : ICrudRepository<TEntity> where TEntity : OpenHotelEntity
{
    private static List<TEntity> _storage = new();

    public virtual Task Create(TEntity entity)
    {
        _storage.Add(entity);
        return Task.CompletedTask;
    }

    public virtual Task<TEntity> GetOne(Guid id)
    {
        var entity = _storage.First(e => e.Id == id);
        return Task.FromResult(entity);
    }

    public virtual Task<IEnumerable<TEntity>> GetAll()
    {
        return Task.FromResult(_storage.AsEnumerable());
    }

    public virtual Task<TEntity> Update(TEntity entity)
    {
        var index = _storage.FindIndex(e => e.Id == entity.Id);
        _storage[index] = entity;
        return Task.FromResult(entity);
    }

    public virtual Task Delete(TEntity entity)
    {
        _storage.RemoveAll(e => e.Id == entity.Id);
        return Task.CompletedTask;
    }
}
