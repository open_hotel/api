using Core.Entities;
using Core.Persistence;
using Infrastructure.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories;

public class ReservationRepository : ICrudRepository<Reservation>
{
    private readonly ReservationContext _context;

    public ReservationRepository(ReservationContext context)
    {
        _context = context;
    }
    public async Task Create(Reservation newObject)
    {
        _context.Reservations.Add(newObject);
        await _context.SaveChangesAsync();
    }

    public async Task<Reservation> GetOne(Guid id)
    {
        var r = await _context.Reservations.FindAsync(id);
        return r ?? throw new ArgumentException("Tried to retrieve a Reservation by non-existing ID");
    }

    public async Task<IEnumerable<Reservation>> GetAll()
    {
        return await _context.Reservations.ToListAsync();
    }

    public async Task<Reservation> Update(Reservation existingObject)
    {
        _context.Entry(existingObject).State = EntityState.Modified;
        await _context.SaveChangesAsync();
        return existingObject;
    }

    public async Task Delete(Reservation existingObject)
    {
        _context.Reservations.Remove(existingObject);
        await _context.SaveChangesAsync();
    }
}