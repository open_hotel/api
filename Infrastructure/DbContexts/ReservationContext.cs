using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.DbContexts;

public class ReservationContext : DbContext
{
    public DbSet<Reservation> Reservations { get; set; }
    private readonly IConfiguration _configuration;

    public ReservationContext(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(_configuration.GetConnectionString("dev"));
    }
}