namespace Core.common;

public interface IAsyncCrud<T>
{
    public Task Create(T newObject);
    public Task<T> GetOne(Guid id);
    public Task<IEnumerable<T>> GetAll();
    public Task<T> Update(T existingObject);
    public Task Delete(T existingObject);
}
