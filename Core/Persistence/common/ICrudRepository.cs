using Core.common;
using Core.Entities;

namespace Core.Persistence;

public interface ICrudRepository<TEntity> : IEntityRepository<TEntity>, IAsyncCrud<TEntity>
    where TEntity : OpenHotelEntity
{
}
