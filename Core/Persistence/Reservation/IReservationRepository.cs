using Core.Entities;

namespace Core.Persistence;

public interface IReservationRepository : ICrudRepository<Reservation>
{
}
