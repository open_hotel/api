namespace Core.Entities;

public class Reservation : OpenHotelEntity
{
    public List<string> Accommodations { get; set; } = new();
    public DateOnly StartDate { get; set; }
    public DateOnly EndDate { get; set; }
    public string MainGuest { get; set; } = "";
    public string Payer { get; set; } = "";
    public string Holder { get; set; } = "";
    public int State { get; set; }
    public string Observations { get; set; } = "";
}
