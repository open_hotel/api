namespace Core.Entities;

public abstract class OpenHotelEntity
{
    public Guid Id { get; init; }
}
