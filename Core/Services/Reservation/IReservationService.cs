using Core.Dto.Reservation;

namespace Core.Services;

public interface IReservationService : ICrudService<ReservationDto>
{
}
