using AutoMapper;
using Core.Dto.Reservation;
using Core.Entities;
using Core.Persistence;

namespace Core.Services;

public class ReservationService : CrudService<Reservation, ReservationDto>, IReservationService
{
    public ReservationService(
        ICrudRepository<Reservation> repository,
        IMapper mapper
    ) : base(repository, mapper)
    {
    }

    public override Task Create(ReservationDto dto)
    {
        var creationTask = base.Create(dto);
        return creationTask;
    }
}
