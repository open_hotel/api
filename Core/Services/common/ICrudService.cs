using Core.common;
using Core.Dto;

namespace Core.Services;

public interface ICrudService<TDto> : IDtoService<TDto>, IAsyncCrud<TDto>
    where TDto : OpenHotelDto
{
}
