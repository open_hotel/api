using AutoMapper;
using Core.Dto;
using Core.Entities;
using Core.Persistence;

namespace Core.Services;

public abstract class CrudService<TEntity, TDto> : ICrudService<TDto>
    where TEntity : OpenHotelEntity
    where TDto : OpenHotelDto
{
    protected readonly ICrudRepository<TEntity> Repository;
    protected readonly IMapper Mapper;

    protected CrudService(ICrudRepository<TEntity> repository, IMapper mapper)
    {
        Repository = repository;
        Mapper = mapper;
    }


    public virtual async Task Create(TDto dto)
    {
        var entity = Mapper.Map<TDto, TEntity>(dto);
        await Repository.Create(entity);
    }

    public virtual async Task<TDto> GetOne(Guid id)
    {
        var entity = await Repository.GetOne(id);
        return Mapper.Map<TEntity, TDto>(entity);
    }

    public virtual async Task<IEnumerable<TDto>> GetAll()
    {
        var entities = await Repository.GetAll();
        return entities.Select(e => Mapper.Map<TEntity, TDto>(e));
    }

    public virtual async Task<TDto> Update(TDto dto)
    {
        var newEntity = Mapper.Map<TDto, TEntity>(dto);
        newEntity = await Repository.Update(newEntity);
        return Mapper.Map<TEntity, TDto>(newEntity);
    }

    public virtual async Task Delete(TDto existingObject)
    {
        var entity = Mapper.Map<TDto, TEntity>(existingObject);
        await Repository.Delete(entity);
    }
}
