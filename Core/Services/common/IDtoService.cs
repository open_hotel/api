using Core.Dto;

namespace Core.Services;

public interface IDtoService<TDto> : IOpenHotelService
    where TDto : OpenHotelDto
{
}
