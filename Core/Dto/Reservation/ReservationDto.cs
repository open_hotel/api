namespace Core.Dto.Reservation;

public record ReservationDto : OpenHotelDto
{
    public List<string> Accommodations { get; init; }
    public DateOnly StartDate { get; init; }
    public DateOnly EndDate { get; init; }
    public string MainGuest { get; init; }
    public string Payer { get; init; }
    public string Holder { get; init; }
    public int State { get; init; }
    public string Observations { get; init; }
}
