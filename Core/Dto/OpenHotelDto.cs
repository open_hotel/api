namespace Core.Dto;

public abstract record OpenHotelDto
{
    public Guid Id { get; init; }
}
