using AutoMapper;
using Core.Dto.Reservation;
using Core.Entities;

namespace Core.Helpers.Mapping.Configurations;

public class AutoMapperConfig : Profile
{
    public AutoMapperConfig()
    {
        CreateMap<Reservation, ReservationDto>().ReverseMap();
    }
}
