namespace Core.Helpers.Mapping;

internal interface IMapper<TEntity, TDto>
{
    internal TEntity ToEntity(TDto dto);

    internal TDto ToDto(TEntity entity);
}
